package com.example.SecurityApp.service;

import com.example.SecurityApp.dto.JwtResponseDto;
import com.example.SecurityApp.dto.LoginDto;
import com.example.SecurityApp.dto.RegisterRequestDto;

public interface UserService {
    JwtResponseDto register(RegisterRequestDto registerRequestDto);

    void login(LoginDto loginDto, String token);
}
