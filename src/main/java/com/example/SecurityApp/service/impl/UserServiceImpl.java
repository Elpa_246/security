package com.example.SecurityApp.service.impl;

import com.example.SecurityApp.config.JwtService;
import com.example.SecurityApp.dto.JwtResponseDto;
import com.example.SecurityApp.dto.LoginDto;
import com.example.SecurityApp.dto.RegisterRequestDto;
import com.example.SecurityApp.model.Authority;
import com.example.SecurityApp.model.User;
import com.example.SecurityApp.model.UserAuthority;
import com.example.SecurityApp.repository.UserRepo;
import com.example.SecurityApp.service.UserService;
import io.jsonwebtoken.Claims;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserRepo userRepo;
    private final BCryptPasswordEncoder passwordEncoder;
    private final JwtService jwtService;
    @Override
    public JwtResponseDto register(RegisterRequestDto registerRequestDto) {
        userRepo.findByName(registerRequestDto.getUsername()).ifPresent(user -> {
            throw new RuntimeException("User already exists");
        });
        if (!registerRequestDto.getPassword().equals(registerRequestDto.getRepeatPassword())) {
            throw new RuntimeException("Passwords are different");
        }
        User user = User.builder()
                .name(registerRequestDto.getUsername())
                .password(passwordEncoder.encode(registerRequestDto.getRepeatPassword()))
                .authorities(Collections.singletonList(Authority.builder()
                        .userAuthority(UserAuthority.USER)
                        .build()))
                .build();
        User saveUser = userRepo.save(user);
        String token = jwtService.generateToken(saveUser);
        return JwtResponseDto.builder()
                .jwt(token)
                .build();
    }

    @Override
    public void login(LoginDto loginDto,String token) {
        userRepo.findByName(loginDto.getUsername()).ifPresent(user -> {
            System.out.println(user.getPassword());
        });
        String trimToken = token.substring("Bearer ".length()).trim();
        Claims claims = jwtService.parseToken(trimToken);
        List<String> list = claims.get("authority", List.class);
        System.out.println(list);
    }
}
