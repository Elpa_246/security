package com.example.SecurityApp.model;

public enum UserAuthority {
    PUBLIC,
    USER,
    ADMIN
}
