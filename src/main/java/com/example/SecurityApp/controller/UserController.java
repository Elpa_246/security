package com.example.SecurityApp.controller;

import com.example.SecurityApp.dto.JwtResponseDto;
import com.example.SecurityApp.dto.LoginDto;
import com.example.SecurityApp.dto.RegisterRequestDto;
import com.example.SecurityApp.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import static org.springframework.http.HttpStatus.OK;

@RestController
@RequiredArgsConstructor
@RequestMapping("/security")
public class UserController {

    private final UserService userService;

    @PostMapping("/register")
    public ResponseEntity<JwtResponseDto> register(@RequestBody RegisterRequestDto registerRequestDto){
        return new ResponseEntity<JwtResponseDto>(userService.register(registerRequestDto),OK);
    }

    @PostMapping("/login")
    public String login(@RequestBody LoginDto loginDto, @RequestHeader("Authorization") String token){
        userService.login(loginDto,token);
        return "Successfully login";
    }
}
