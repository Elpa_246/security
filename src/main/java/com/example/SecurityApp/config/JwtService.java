package com.example.SecurityApp.config;

import com.example.SecurityApp.model.User;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.io.Decoders;
import io.jsonwebtoken.security.Keys;
import jakarta.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.security.Key;
import java.time.Duration;
import java.time.Instant;
import java.util.Date;
import java.util.stream.Collectors;

@Component
public class JwtService {
    private Key key;
    @Value("bWljcm9zZXJ2aWNlczE4IG1pY3Jvc2VydmljZXMxOCBtaWNyb3NlcnZpY2VzMTg=")
    private String secret;
    private byte[] bytes;

    @PostConstruct
    public void init(){
        bytes = Decoders.BASE64.decode(secret);
        key= Keys.hmacShaKeyFor(bytes);
    }

    public Claims parseToken(String token){
        return Jwts.parserBuilder()
                .setSigningKey(key)
                .build()
                .parseClaimsJws(token)
                .getBody();
    }

    public String generateToken(User user){
        return Jwts.builder()
                .setIssuedAt(new Date())
                .setExpiration(Date.from(Instant.now().plus(Duration.ofMinutes(5))))
                .setSubject(user.getName())
                .signWith(key, SignatureAlgorithm.HS256)
                .claim("authority", user.getAuthorities().stream().map(authority -> authority.getUserAuthority().name()).collect(Collectors.toList()))
                .compact();
    }
}
